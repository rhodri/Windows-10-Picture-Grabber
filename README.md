# Windows 10 Pic Grabber

Know those lovely photos that Windows 10 puts on your lockscreen?

Ever wanted to keep them forever?

Run this script.

## Usage

Either clone the repo to your local computer or click [here](https://gitlab.com/rhodri/Windows-10-Picture-Grabber/raw/master/Win10PicGrabber.ps1) and download the contents of the file and save it as `Win10PicGrabber.ps1` (make sure there isn't a `.txt` extension appended to the end).

Right click on downloaded file and select `Run with Powershell`. Take a look on your desktop for a folder called `Lock-Screen-Photos`.

If the script doesn't run or throws an error you may need to run `Set-ExecutionPolicy -Scope Process -ExecutionPolicy Bypass` in order to get around the default security policies.

## Changing the default save location

Look for the following line:

```ps
$OutputDirectory = $env:USERPROFILE + "\Desktop\Lock-Screen-Photos"
```

Change `$OutputDirectory` to be whatever you want it to be.