$Source = $env:LOCALAPPDATA + "\Packages\Microsoft.Windows.ContentDeliveryManager_cw5n1h2txyewy\LocalState\Assets"
$OutputDirectory = [Environment]::GetFolderPath("Desktop") + "\Lock-Screen-Photos"

# If exists delete the output directory, ignore error if it doesn't.
Remove-Item $OutputDirectory -Recurse -ErrorAction Ignore

Set-Location -Path $Source
New-Item -ItemType Directory -Force -Path $OutputDirectory

# Copy files larger then $SizeThreshold to folder on the Desktop. Threshold currently set to 100kb because anything
# smaller is usually an icon or something we won't be interested in. 
$SizeThreshold = 100000
Get-ChildItem -recurse -ErrorAction "SilentlyContinue" | 
    Where-Object { !($_.PSIsContainer) -and $_.Length -gt $SizeThreshold } | 
    ForEach-Object {
    Copy-Item $_.fullname -Destination $OutputDirectory
}
# Add .jpg extension to files in $OutputDirectory
Set-Location -Path $OutputDirectory
Get-ChildItem  | 
    ForEach-Object {
        $Extension = "jpg"
        $NewName = ("{0}.{1}" -f $_.FullName, $Extension)
        Rename-Item  -Path $_.FullName -NewName $NewName 
    }